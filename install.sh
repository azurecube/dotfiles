#! /bin/bash

OS=$(grep 'ID_LIKE' ../arch/etc/os-release | cut -d'=' -f2)

case ${OS} in 
  archlinux ) pacman -Syu git ansible --noconfirm      ;;
  debian    ) sudo apt-get -y install git ansible      ;;
  *         ) echo "Unknown OS exitting..."; return -1 ;;
esac

git clone https://azurecube@bitbucket.org/azurecube/dotfiles.git

# link and setup when clone succeeded
if [ $? == 0 ]; then
  cd dotfiles
  bash link.sh
  bash setup.sh
fi
